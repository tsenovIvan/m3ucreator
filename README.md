# m3ucreator
Simple M3U playlist creator

A simple scrip that creates a M3U playslist from a directory.  
It will consider any sub-directories as well.

It is run from a console and accepts the directory path as a parameter.  
>python M3U_creator.py "D:\Music\Some-band"
