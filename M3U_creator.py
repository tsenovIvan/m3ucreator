# Author: Ivan Tsenov
# License -- GPLv2
#
# For more information see full license here  https://www.gnu.org/licenses/gpl-2.0.html
#
# Reads directory and writes an M3U playlist for it
#
import os;
import sys;

file_types = ['.mp3', '.flac', '.aac', '.wav']
dir_separator = os.sep
base_dir_length = None

#Recursively read directory and writes songs into the file
def fill_dir(file, dir, base_dir):
		
	list = os.listdir(dir)
	
	for item in list:
		#If item is a directory itself, call recur
		if os.path.isdir(dir + dir_separator + item):
			fill_dir(file, dir + dir_separator + item, base_dir)
		#If item is media file writes it to disk
		if any(x in item for x in file_types):
			#If in a sub directory write only the relative path
			if dir != base_dir:
				file.write(dir[base_dir_length+1:] + dir_separator)
			file.write(item + "\n")


if len(sys.argv) > 1:

	# Checks to see if this is a real directory
	if os.path.isdir(sys.argv[1]):

		base_dir_length = len(sys.argv[1])
		
		# Opens file for writing
		file = open (sys.argv[1] + dir_separator + os.path.basename(sys.argv[1]) + ".m3u", "w");	
		file.write("#M3U\n");
		
		fill_dir(file, sys.argv[1], sys.argv[1]);
		
		file.close();
	else:
		print ("This is not a directory!")
		
else:
	print ("Give a directory for parameter")
